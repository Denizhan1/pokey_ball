﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Manager : MonoBehaviour
{
    public int score = 0;
    private void Start()
    {
       score = PlayerPrefs.GetInt("score", score);
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetSceneAt(0).name);
        PlayerPrefs.SetInt("score", 0);
    }
    public void Next_Level()
    {
        PlayerPrefs.SetInt("score", score);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
