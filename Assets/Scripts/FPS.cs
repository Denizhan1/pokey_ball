﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPS : MonoBehaviour
{
    float deltaTime = 0.0f;
    GUIStyle style;
    Rect fpsRect;
    float fps;
    int w = Screen.width, h = Screen.height;
    Rect rect;
    private void Start()
    {
        style = new GUIStyle();
        rect = new Rect(Screen.width * 3 / 4, 100, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 5 / 100;
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        StartCoroutine(RecalculateFPS());
    }
    private IEnumerator RecalculateFPS()
    {
        while (true)
        {
            fps = 1 / Time.deltaTime;
            yield return new WaitForSeconds(1);
        }
    }
    void OnGUI()
    {
        float msec = 1 / fps * 1000.0f;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        GUI.Label(rect, text, style);
    }
}
