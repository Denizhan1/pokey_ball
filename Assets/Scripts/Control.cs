﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Control : MonoBehaviour
{
    public bool flight = false;
    public TMPro.TextMeshProUGUI Height;
    public TMPro.TextMeshProUGUI Score;
    public Transform ball;
    public GameObject levelmanager;

    private float m_previousY;
    private float m_previousX;
    public float toplamDelta;
    public Animator animator;
    public bool throwing = false;
    private bool play = true;
    private float delta;
    private float speed;
    public bool ExtraForce = false;
    public GameObject Trail_Slow, Trail_Fast,end;

    public AudioSource stick_sound;
    public AudioSource platform_sound;
    public AudioSource obstacle_sound;
    public AudioSource extra_force_sound;
    public AudioSource finish_sound;
    public AudioSource target_sound;

    void Start()
    {
        animator.GetComponent<Animator>();
        animator.speed = 0;
        end.SetActive(false);
        Trail_Fast.SetActive(false);
        Trail_Slow.SetActive(true);
        //GetComponent<CapsuleCollider>().enabled = false;
    }

    private void Update()
    {
        if (this.animator.GetCurrentAnimatorStateInfo(0).IsName("spin"))
        {
            Debug.Log("spinnn");
        }
        Height.text = "Height: " + Convert.ToInt32(transform.position.y).ToString() + " m";
#if !MOBILE_INPUT
        Control_PC();
#else
            Control_Mobile();
#endif
    }
    private void Control_PC()
    {
        if (play)
        {
            if (!flight)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    throwing = true;
                    Debug.Log("firstclick");
                    animator.speed = 0;
                    animator.SetTrigger("Bend");
                    toplamDelta = 0.0f;
                    delta = 0;
                    m_previousY = Input.mousePosition.y;
                }
                if (Input.GetMouseButton(0) && m_previousY > Input.mousePosition.y)
                {
                    delta = (m_previousY - Input.mousePosition.y);
                    m_previousY = Input.mousePosition.y;
                    animator.speed = delta * delta / 10000;
                    toplamDelta += delta;
                    if (toplamDelta >= 300)
                        toplamDelta = 300;
                }
                if (throwing && Input.GetMouseButtonUp(0))
                {
                    stick_sound.Play();
                    Debug.Log("firstclickUpp");
                    animator.ResetTrigger("Bend");
                    animator.speed = toplamDelta / 60;
                    animator.SetTrigger("Release");
                    animator.speed = 1;
                    Invoke("Force", 0.05f);
                    delta = 0;
                    throwing = false;
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    animator.SetTrigger("Tap");
                    animator.SetTrigger("Coll");
                }
                if (Input.GetMouseButtonUp(0))
                {
                    Debug.Log("secondclickUpp");
                }
            }
        }
    }
    public void Force()
    {
        float force = toplamDelta / 5;
        flight = true;
        ball.GetComponent<Rigidbody>().useGravity = true;
        if (ExtraForce)
        {
            extra_force_sound.Play();
            Trail_Slow.SetActive(false);
            Trail_Fast.SetActive(true);
            force *= 2;
            ExtraForce = false;
        }
        GetComponent<Rigidbody>().velocity = new Vector3(0, force, 0);
        //ball.GetComponent<Rigidbody>().AddForce(ball.up * toplamDelta * 20);
    }
    //void OnTriggerEnter2D(Collider2D col)
    //{
    //    Debug.Log("trigger");
    //    if (col.gameObject.tag == "inside")
    //    {
    //        Debug.Log("inside");
    //        ExtraForce = true;
    //    }
    //    if (col.gameObject.tag == "mid")
    //    {
    //        Debug.Log("mid");
    //        ExtraForce = true;
    //    }
    //    if (col.gameObject.tag == "outside")
    //    {
    //        Debug.Log("outside");
    //        ExtraForce = true;
    //    }
    //}
    private void OnTriggerEnter(Collider coll)
    {
        speed = GetComponent<Rigidbody>().velocity.y;
        animator.ResetTrigger("Coll");


        if (coll.gameObject.tag == "Platform")
        {
            platform_sound.Play();
            Trail_Fast.SetActive(false);
            Trail_Slow.SetActive(true);

            GetComponent<Rigidbody>().velocity = Vector3.zero;
            Debug.Log("Platform");
            flight = false;
            //GetComponent<Animator>().ResetTrigger("Coll");
            GetComponent<Rigidbody>().useGravity = false;
            if (speed > 0)
            {
                animator.SetTrigger("OscUp");
            }
            else
            {
                animator.SetTrigger("OscDown");
            }
        }
        if (coll.gameObject.tag == "Obstacle")
        {
            obstacle_sound.Play();
            Trail_Fast.SetActive(false);
            Trail_Slow.SetActive(true);

            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().useGravity = true;
            flight = true;
            Debug.Log("Obstacle");
            animator.SetTrigger("Coll");
        }
        if (coll.gameObject.tag == "Ground")
        {
            Debug.Log("Ground");
            FindObjectOfType<Manager>().Restart();
        }
        if (coll.gameObject.tag == "coin")
        {
            levelmanager.GetComponent<Manager>().score += 100;
            Score.text = "Score: " + levelmanager.GetComponent<Manager>().score;
            coll.transform.parent = null;
            Destroy(coll.gameObject);
        }

        if (coll.gameObject.tag == "inside")
        {
            target_sound.Play();
            ExtraForce = true;
            levelmanager.GetComponent<Manager>().score += 300;
            Score.text = "Score: " + levelmanager.GetComponent<Manager>().score;
        }
        if (coll.gameObject.tag == "mid")
        {
            levelmanager.GetComponent<Manager>().score += 200;
            Score.text = "Score: " + levelmanager.GetComponent<Manager>().score;
        }
        if (coll.gameObject.tag == "outside")
        {
            levelmanager.GetComponent<Manager>().score += 100;
            Score.text = "Score: " + levelmanager.GetComponent<Manager>().score;

        }


        if (coll.gameObject.tag == "level1")
        {
            finish_sound.Play();
            Trail_Fast.SetActive(false);
            Trail_Slow.SetActive(true);
            play = false;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().useGravity = false;
            StartCoroutine(End_Game());
        }

        if (coll.gameObject.tag == "level2")
        {
            finish_sound.Play();
            Trail_Fast.SetActive(false);
            Trail_Slow.SetActive(true);
            play = false;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().useGravity = false;
            StartCoroutine(End_Game());
        }

        if (coll.gameObject.tag == "level3")
        {
            finish_sound.Play();
            Trail_Fast.SetActive(false);
            Trail_Slow.SetActive(true);
            play = false;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().useGravity = false;
            StartCoroutine(End_Game());
        }

        if (coll.gameObject.tag == "level4")
        {
            finish_sound.Play();
            Trail_Fast.SetActive(false);
            Trail_Slow.SetActive(true);
            play = false;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().useGravity = false;
            StartCoroutine(End_Game());
        }

        if (coll.gameObject.tag == "level5")
        {
            finish_sound.Play();
            Trail_Fast.SetActive(false);
            Trail_Slow.SetActive(true);
            play = false;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().useGravity = false;
            StartCoroutine(End_Game());
        }

        if (coll.gameObject.tag == "level6")
        {
            finish_sound.Play();
            Trail_Fast.SetActive(false);
            Trail_Slow.SetActive(true);
            play = false;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().useGravity = false;
            StartCoroutine(End_Game());
        }


        if (coll.gameObject.tag == "End")
        {
            FindObjectOfType<Manager>().Next_Level();
        }
    }


    IEnumerator End_Game()
    {
        yield return new WaitForSeconds(2);
        GetComponent<Rigidbody>().useGravity = true;
        animator.SetTrigger("Coll");
        end.SetActive(true);
    }
}
