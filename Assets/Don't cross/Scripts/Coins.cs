﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{
    private float speed = 4.0f;
    public AudioClip coin;
    void Update()
    {
        transform.Rotate(0, 100 * Time.deltaTime, 0);
        //Debug.Log(transform.position.x);
        if (transform.position.x >= 257.46f)
        {
            speed = -4.0f;
        }
        if (transform.position.x <= 241.46f)
        {
            speed = 4.0f;
        }
        transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y, transform.position.z);
    }
    private void OnTriggerEnter(Collider coll)
    {
        AudioSource.PlayClipAtPoint(coin, transform.position);
    }
}